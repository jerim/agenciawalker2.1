@extends('layouts.app')
@section('content')
<div class="card pmd-card">
  <div class="card-body d-flex flex-row">
    <div class="media-body">
        <h2 class="card-title">{{$producto->nombre}} desde {{$producto->precio}} €</h2>
        <p class="card-subtitle">{{$producto->descripcion}}</p>
        <p class="card-subtitle">Lo que incluye en el viaje es el vuelo más el transporte desde el aeropueto al hotel.</p>
        <p class="card-subtitle">Lo que no incluye en el viaje son los restaurantes ,tiendas ni las entradas para los museos y monumentos historios.
        Si lo desean pueden obtenerlos en alguna excursión de la agencia.</p>
    </div>
     <img src="/imagenes/productos/{{$producto->rutaImg}}" width="112" height="112" />
  </div>
  <div class="card-footer">
    <!-- <a href="/añade/{{$producto->cod_viajes}}" class="btn btn-success">Guardar  en cesta</a>-->

    <a href="/agencia" class="btn btn-primary">Volver a home</a>
  </div>
</div>

@endsection
